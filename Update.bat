@echo off
title Soundpac Updater
echo Updating your soundpack, please wait...
git pull
if ERRORLEVEL 1 (
echo Git exited with error code %errorlevel%, and probably failed.
) else (
echo Your soundpack was successfully updated. Please either close and reopen Vipmud, or use SpReload.
)
echo Cleaning your soundpack folder, please wait...
git clean -f
if ERRORLEVEL 1 (
echo There was a problem cleaning your soundpack directory.
) else (
echo Soundpack directory successfully cleaned.
)
pause
